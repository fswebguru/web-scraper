### Web Scraper

*Data processing scripts based on python*

---

** Prerequisites **

*NOTE: Ubuntu 16.04, Python version > 3.5, Requests, BeautifulSoup, Pandas, Selenium*

---

** Installation **

- `sudo apt-get install python3-pip`
- `sudo pip3 install virtualenv`
- `cd ~/web-scraper`
- `virtualenv env`
- `source env/bin/activate`
- `pip3 install requests bs4 selenium pandas`

---

** Run **

1). Scrape the non-authorized web page and export the details of product to the JSON file.

- `python web-s.py`

2). Scrape the authenticated web page with the login credentials and print the html content.

- `python disneyplus.py`
