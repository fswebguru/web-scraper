import requests
from bs4 import BeautifulSoup

login_url = 'https://www.disneyplus.com/login'
homepage_url = 'https://www.disneyplus.com'

# Fill in your details here to be posted to the login form.
payload = {
    'inUserName': 'USER-EMAIL',
    'inUserPass': 'USER-PASSWORD'
}

http_proxy  = "http://dlitwi:HelloSir@51.89.11.97:29842"
https_proxy = "http://dlitwi:HelloSir@51.89.11.97:29842"

proxyDict = { 
              "http"  : http_proxy, 
              "https" : https_proxy
            }

# Use 'with' to ensure the session context is closed after use.
with requests.Session() as s:
    p = s.post(login_url, data=payload, proxies=proxyDict)    
    # print the html returned or something more intelligent to see if it's a successful login page.
    soup = BeautifulSoup(p.content, 'html.parser')
    with open('dp-login.html', 'w') as f:
	    for row in soup:
	        f.write("%s\n" % str(row))

    # An authorised request.
    r = s.get(homepage_url, proxies=proxyDict)
    soup = BeautifulSoup(r.content, 'html.parser')
    with open('dp-homepage.html', 'w') as f:
	    for row in soup:
	        f.write("%s\n" % str(row))
